import QtQuick 2.9
import QtPositioning 5.2

import "../transport-api.js" as Transport
import "../generalfunctions.js" as GeneralFunctions

Item {
    id: positionSourceItem
    
    property var active: false
    property var isValid: positionSource.isValid
    property var keepActive: false
    property var valid: positionSource.valid
    property var position: positionSource.position
    property var timeoutLength: 10000
    
    function append(func) {
        return positionSource.append(func);
    }
    
    function close(index) {
        return positionSource.close(index);
    }
    
    PositionSource {
        id: positionSource
        updateInterval: functionsToRunOnUpdate.length > 0 ? 1000 : 10000
        active: positionSourceItem.active
        
        property var functionsToRunOnUpdate: []
        property var isValid: false
        property var keepActive: positionSourceItem.keepActive
        
        function append(func) {
            Transport.eventListener.update({
                name: "geo-search",
                response: true
            });
            
            if(keepActive) {
                start();
            }
            else {
                update();
            }
            
            if(position.coordinate.isValid) {
                if(isValid) {
                    console.log("Position found, executing function");
                }
                if(isValid) {
                    console.error("Position search timed out, executing function");
                }
                func(positionSource);
            }
            else {
                functionsToRunOnUpdate.push(func);
                
                if(keepActive) {
                    start();
                }
                else {
                    update();
                }
            }
            
            timeoutTimer.restart();
            return functionsToRunOnUpdate.length - 1;
        }
        
        function close(index) {
            console.log("close(index)", index);
            if(index >= 0 && functionsToRunOnUpdate.length > index) {
                console.log("closing");
                isValid = false;
                functionsToRunOnUpdate[index](positionSource);
                functionsToRunOnUpdate.splice(index, 1);
            }
            
            if(functionsToRunOnUpdate.length === 0) {
                Transport.eventListener.update({
                    name: "geo-search",
                    response: false
                });
            }
        }
        
        function runAll() {
            console.log("Position found, executing backlog function");
            for(var i = 0; i < functionsToRunOnUpdate.length; i++) {
                functionsToRunOnUpdate[i](positionSource);
            }
            functionsToRunOnUpdate = [];
            
            Transport.eventListener.update({
                name: "geo-search",
                response: false
            });
        }
        
        function updateTimeoutAction() {
            isValid = false;
            runAll();
        }
        
        onUpdateTimeout: {
            updateTimeoutAction();
        }
        
        onPositionChanged: {
            if(position.coordinate.isValid) {
                isValid = true;
                runAll();
                
                if(active) {
                    if(Transport.transportOptions.dbConnection.usable) {
                        Transport.transportOptions.saveDBSetting("last-geo-positionX", position.coordinate.latitude);
                        Transport.transportOptions.saveDBSetting("last-geo-positionY", position.coordinate.longitude);
                    }
                }
                
                if(!keepActive) {
                    active = false;
                }
            }
            else {
                isValid = false;
            }
        }
        
        onKeepActiveChanged: {
            if(!active && keepActive) {
                start();
            }
            else if(active && !keepActive) {
                if(functionsToRunOnUpdate.length === 0) {
                    stop();
                }
            }
        }
        
        onActiveChanged: {
            if(!active && keepActive) {
                start();
            }
            console.log("PositionSourceItem", "geo-location-search", active ? "Started" : "Stopped");
        }
    }
    
    Timer {
        id: timeoutTimer
        interval: positionSourceItem.timeoutLength
        repeat: false
        running: false
        triggeredOnStart: false

        onTriggered: {
            positionSource.updateTimeoutAction();
        }
    }
}
